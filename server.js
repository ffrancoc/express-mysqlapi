const mysql = require('mysql')
const express = require('express')
const bodyParser = require('body-parser')
const { urlencoded } = require('body-parser')

const app = express()
app.use(bodyParser.json())
app.use(urlencoded({ extended: true }))

const port = 4440

app.get('/', (req, res) => {
    res.send('API SERVER MySQL')
})

app.get('/usuarios', (req, res) => {
    var conn = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'db'
    })

    conn.connect((err) => {
        if (err) {
            console.log(`Error al conectar a la base de datos, ${err}`)
        } else {
            console.log('Conectado a la base de datos')
        }
    })

    conn.query('SELECT * FROM usuario;', (error, result, fields) => {
        if (result) {
            res.status(200)
            res.json(result)
            //res.send(result)
            //console.log(result)
        } else {
            res.status(404)
            res.json({ msg: 'Hay un problema con la consulta' })
        }
    })
    conn.end((err) => {
        if (err) {
            console.log(`Error al cerra la base de datos, ${err}`)
        } else {
            console.log('Base de datos cerrada correctamente')
        }
    })

    //res.send(resultados)
})

app.get('/usuario', (req, res) => {
    var conn = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'password',
        database: 'db'
    })

    conn.connect((err) => {

    })

    conn.query('SELECT * FROM usuario WHERE id_usuario = ' + req.query.id, (error, result, fields) => {
        if (result) {
            res.status(200)
            res.json(result)
        } else if (!result.length) {
            res.json({ msg: 'Usuario no encontrado' })
        } else {
            res.status(404)
            res.json({ msg: `Error, ${error}` })
        }
    })

    conn.end((err) => {

    })

    console.log(`Id usuario ${req.query.id}`)
})


app.listen(port, () => {
    console.log("El servidor se ha iniciado en http://localhost:4440")
})
